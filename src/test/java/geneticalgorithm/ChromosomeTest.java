package geneticalgorithm;

import client.Coordinates;
import client.Ride;
import org.junit.Assert;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ChromosomeTest {

    @Test
    public void testGetDestinations() throws Exception {
        Chromosome chromosome = new Chromosome();
        Ride rideOne = mock(Ride.class);
        when(rideOne.getDestination()).thenReturn(new Coordinates(12.7, 15.2));
        chromosome.addGene(rideOne);
        chromosome.addGene(rideOne);
        chromosome.addGene(rideOne);
        Ride rideTwo = mock(Ride.class);
        when(rideTwo.getDestination()).thenReturn(new Coordinates(16.2, 15.6));
        chromosome.addGene(rideTwo);
        chromosome.addGene(rideTwo);

        Assert.assertEquals(2, chromosome.getDestinations().size());
    }

    @Test
    public void testGetPotentialDrivers() throws Exception {
        Chromosome chromosome = new Chromosome();
        Ride potentialDriver = mock(Ride.class);
        when(potentialDriver.isDriver()).thenReturn(true);
        chromosome.addGene(potentialDriver);
        chromosome.addGene(potentialDriver);
        chromosome.addGene(potentialDriver);
        chromosome.addGene(potentialDriver);
        Ride strictlyPassenger = mock(Ride.class);
        when(strictlyPassenger.isDriver()).thenReturn(false);
        chromosome.addGene(strictlyPassenger);
        chromosome.addGene(strictlyPassenger);
        chromosome.addGene(strictlyPassenger);

        Assert.assertEquals(4, chromosome.getPotentialDrivers().size());
    }
}
