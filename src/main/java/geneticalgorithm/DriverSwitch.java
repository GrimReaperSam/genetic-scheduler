package geneticalgorithm;


import client.Ride;

import java.util.List;
import java.util.Random;

public class DriverSwitch {
	private static final double INITIAL_DRIVER_SWITCH_PROBABILITY = 0.5;

	private double driverSwitchProbability;

	public DriverSwitch() {
		this.setDriverSwitchProbability(INITIAL_DRIVER_SWITCH_PROBABILITY);
	}


	public void setDriverSwitchProbability(double driverSwitchProbability) {
		this.driverSwitchProbability = driverSwitchProbability;
	}

	public double getDriverSwitchProbability() {
		return driverSwitchProbability;
	}

	public boolean driverSwitch(List<Chromosome> population, Chromosome chromosome) throws Exception {
		Chromosome testChromosome = new Chromosome(chromosome);
		int chromosomeIndex = population.indexOf(chromosome);
		List<Ride> potentialDrivers = testChromosome.getPotentialDrivers();
		Ride toSwitch = potentialDrivers.get(new Random().nextInt(potentialDrivers.size()));
		testChromosome.swap(testChromosome.getDriver(), toSwitch);
		if (testChromosome.getFitness() > chromosome.getFitness()) {
			population.set(chromosomeIndex, testChromosome);
			return true;
		}
		return false;
	}
}
