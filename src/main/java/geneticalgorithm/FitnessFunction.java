package geneticalgorithm;

import client.*;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Minutes;

import java.util.List;

public class FitnessFunction {

	private static double FAIRNESS_CONSTANT = 1.2;

	public static double evaluateTotal(Chromosome chromosome) throws Exception {
		double increaseInDistance = increaseInDistance(chromosome);
		double fairness = fairness(chromosome);
		double timePenalty = timePenalty(chromosome);
		double preferenceCost = preferenceCost(chromosome);
		double fitness = 100 - increaseInDistance - fairness - timePenalty - preferenceCost;
		if (chromosome.size() > 1) {
			return fitness;
		} else {
			return 50;
		}
	}

	public static double evaluate(Chromosome chromosome) throws Exception {
		double increaseInDistance = increaseInDistance(chromosome);
		double fairness = fairness(chromosome);
		double timePenalty = timePenalty(chromosome);
		double preferenceCost = preferenceCost(chromosome);
		double fitness = 100 - increaseInDistance - fairness - timePenalty - preferenceCost;
		if (chromosome.size() > 1) {
			return fitness;
		} else {
			return 50;
		}
	}

	public static double increaseInDistance(Chromosome chromosome) throws Exception {
		Ride driver = chromosome.getDriver();
		double originalDistance = driver.getOrigin().distance(driver.getDestination());
		double newDistance = chromosome.getPathCost();
		double d = (newDistance - originalDistance) / originalDistance * 100;
		if (d < 20) {
			return d;
		} else {
			return d;
		}
	}

	public static double timePenalty(Chromosome chromosome) throws Exception {
		Ride driver = chromosome.getDriver();
		double penalty = 0;
		List<Coordinates> path = chromosome.getPath();
		List<Coordinates> destinations = chromosome.getDestinations();
		for (Coordinates destination : destinations) {
			DateTime arrivalTime = getArrivalTime(destination, path, driver);
			for (Ride ride : chromosome.getGenes()) {
				if (ride.getDestination().equals(destination)) {
					penalty += getPenalty(arrivalTime, ride.getArrival());
				}
			}
		}
		return penalty;
	}

	private static int getPenalty(DateTime arrivalTime, Interval arrival) {
		if (arrival.contains(arrivalTime)) {
			return 0;
		} else if (arrival.isBefore(arrivalTime)) {
			return 100;
		}
        return Minutes.minutesBetween(arrivalTime, arrival.getStart()).dividedBy(20).getMinutes();
	}

	private static DateTime getArrivalTime(Coordinates destination, List<Coordinates> path, Ride driver) throws Exception {
		int index = 0;
		double totalDistance = 0;
		while (!destination.equals(path.get(index))) {
			totalDistance += path.get(index).distance(path.get(index + 1));
			index++;
		}
        return driver.getDeparture().getStart().plus((long) (totalDistance / 60));
	}

	public static double fairness(Chromosome chromosome) throws Exception {
		Ride driver = chromosome.getDriver();
		if (driver.getUser().getFairness() == 0) {
			return 0;
		} else {
			return Math.pow(FAIRNESS_CONSTANT, driver.getUser().getFairness() + (increaseInDistance(chromosome) / 100.0));
		}
	}

	private static double preferenceCost(Chromosome chromosome) {
		double everybodyCost = everybodyCost(chromosome);
		double driverCost = driverCost(chromosome);
		return everybodyCost + driverCost;
	}

	private static double everybodyCost(Chromosome chromosome) {
		List<Ride> genes = chromosome.getGenes();
		double cost = 0;
		for (int i = 0; i < genes.size(); i++) {
			for (int j = 0; j < genes.size(); j++) {
				if (i != j) {
					Preferences preferences = genes.get(i).getUser().getPreferences();
					if (preferences.isSmokingCondition()) {
						if (genes.get(j).getUser().getPreferences().isSmokes()) {
							cost += preferences.getSmokingPriority();
						}
					}
					if (preferences.isAgeGroupCondition()) {
						if (preferences.getAgeGroup() != genes.get(j).getUser().getPreferences().getAgeGroup()) {
							cost += preferences.getAgeGroupPriority();
						}
					}
				}
			}
		}
		return cost;
	}

	private static double driverCost(Chromosome chromosome) {
		Ride driver = chromosome.getDriver();
		if (chromosome.size() > driver.getUser().getPreferences().getPreferredCarCapacity()) {
			return driver.getUser().getPreferences().getCarCapacityPriority();
		} else {
			return 0;
		}
	}

}
