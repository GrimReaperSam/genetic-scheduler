package geneticalgorithm;

import client.Coordinates;
import client.Ride;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.common.collect.ObservableList;
import initialsolution.PathOptimizerDP;

import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Chromosome {

	private ObservableList<Ride> genes;
	private double fitness;
	private List<Coordinates> path;
	private double pathCost;
	private Boolean changed;

	public Chromosome() throws Exception {
		setGenes(new ArrayList<>());
	}

	public Chromosome(List<Ride> list) throws Exception {
		setGenes(new ArrayListModel<>(list));
	}

	public Chromosome(Chromosome chromosome) throws Exception {
		setGenes(new ArrayListModel<>(chromosome.getGenes()));
	}

	public void setGenes(List<Ride> genes) throws Exception {
		this.genes = new ArrayListModel<>();
		this.genes.addListDataListener(new ListDataListener() {

			@Override
			public void intervalRemoved(ListDataEvent e) {
				changed = true;
			}

			@Override
			public void intervalAdded(ListDataEvent e) {
				changed = true;
			}

			@Override
			public void contentsChanged(ListDataEvent e) {
				changed = true;
			}
		});
		if (!genes.isEmpty()) {
			if (genes.get(0).isDriver() && genes.size() <= 7) {
				this.genes.addAll(genes);
				changed = true;
			} else {
				throw new Exception("First gene is not a driver Panic!!" + genes.size());
			}
		}
	}

	protected void refresh() throws Exception {
		changed = false;
		// fillTables();
		PathOptimizerDP optimizer = new PathOptimizerDP();
		path = optimizer.getPath(this);
		pathCost = optimizer.getPathCost(this);
		fitness = FitnessFunction.evaluate(this);
	}

	public List<Ride> getGenes() {
		return genes;
	}

	public double getFitness() throws Exception {
		if (changed) {
			refresh();
		}
		return fitness;
	}

	public List<Coordinates> getPath() throws Exception {
		if (changed) {
			refresh();
		}
		return path;
	}

	public Double getPathCost() throws Exception {
		if (changed) {
			refresh();
		}
		return pathCost;
	}

	public int size() {
		return genes.size();
	}

	public Ride getGeneAt(int index) {
		return genes.get(index);
	}

	public void addGene(Ride user) {
		genes.add(user);
	}

	public void setGeneAt(int index, Ride user) {
		if (index != 0 || user.isDriver()) {
			genes.set(index, user);
		}
	}

	public Ride getDriver() {
		return genes.get(0);
	}


	public List<Ride> getPotentialDrivers() {
       return genes.stream().filter(Ride::isDriver).collect(Collectors.toList());
	}

	public List<Coordinates> getDestinations() {
       return genes.stream().map(Ride::getDestination).distinct().collect(Collectors.toList());
	}

	public void swap(Ride a, Ride b) throws Exception {
		Collections.swap(genes, genes.indexOf(a), genes.indexOf(b));
	}

	public void swap(Chromosome chosenChromosome, Ride chosenUser, Ride swapped) throws Exception {
		this.setGeneAt(genes.indexOf(swapped), chosenUser);
		chosenChromosome.setGeneAt(chosenChromosome.genes.indexOf(chosenUser), swapped);
	}

	public void print() throws Exception {
        print(new PrintWriter(System.out));
	}

	public void print(PrintWriter out) throws Exception {
		out.println("Chromosome Fitness: " + getFitness());
		out.println("Chromosome Increase In Distance: " + FitnessFunction.increaseInDistance(this));
		out.println("Chromosome penalty: " + FitnessFunction.timePenalty(this));
		out.println("Chromosome Fairness: " + FitnessFunction.fairness(this));
		out.println("Chromosome contains:");
        genes.forEach((ride) -> ride.print(out));
		out.println("\n");
	}
}
