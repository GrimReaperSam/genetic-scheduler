package geneticalgorithm;

import client.Coordinates;
import client.Ride;
import client.RideBuilder;
import client.User;
import initialsolution.InitialSolutions;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Run {

	public static void main(String[] args) throws Exception {
		int drivers = 150;
		int passengers = 250;
		int destinations = 25;
		List<Ride> PD = new ArrayList<>();
		List<Ride> SR = new ArrayList<>();
		List<Coordinates> destination = new ArrayList<>();

		for (int i = 0; i < destinations; i++) {
			destination.add(randomCoordinates());
		}

		for (int i = 0; i < drivers; i++) {
			User user = new User(true);
			Ride ride = user.newRide().origin(randomCoordinates()).destination(destination.get(new Random().nextInt(25))).createRide();
            PD.add(ride);
		}

		for (int j = 0; j < passengers; j++) {
			User user = new User(false);
            Ride ride = user.newRide().origin(randomCoordinates()).destination(destination.get(new Random().nextInt(25))).createRide();
            SR.add(ride);
		}
		InitialSolutions solutions = new InitialSolutions();
		List<List<Chromosome>> initialSolutions = solutions
				.getInitialSolutions(PD, SR);
		List<Chromosome> population = new ArrayList<>(
				initialSolutions.get(2));
		long startTime = System.currentTimeMillis();

		for (int index = 0; index < 1; index++) {
			System.out.println("RUN: " + index);
			PopulationHelper.evolve(population);
			System.out.println("\n\n");
		}

		double minIncD = FitnessFunction.increaseInDistance(population.get(0));
		double maxIncD = FitnessFunction.increaseInDistance(population.get(0));
		double total = 0;
		for (Chromosome c : population) {
			c.print();
			double fitness = FitnessFunction.increaseInDistance(c);
			if (fitness < minIncD && fitness > 0) {
				minIncD = fitness;
			}
			if (fitness > maxIncD) {
				maxIncD = fitness;
			}
			total += fitness;
		}
		long endTime = System.currentTimeMillis();
		System.out.println(minIncD + "   " + maxIncD + "    Avg " + total
				/ population.size());
		System.out.println("That took " + (endTime - startTime) / 60000
				+ " minutes");
	}

	private static Coordinates randomCoordinates() {
		return new Coordinates(new Random().nextInt(101) * (-1)
				^ (new Random().nextInt(3)), new Random().nextInt(101) * (-1)
				^ (new Random().nextInt(3)));
	}
}
