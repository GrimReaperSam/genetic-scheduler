package geneticalgorithm;

import java.util.List;
import java.util.Random;

public class Crossover {
	private static final double INITIAL_CROSSOVER_PROBABILITY = 0.4;

	private double crossoverProbability;

	public Crossover() {
		this.setCrossoverProbability(INITIAL_CROSSOVER_PROBABILITY);
	}

	public void setCrossoverProbability(double swapProbability) {
		this.crossoverProbability = swapProbability;
	}

	public double getCrossoverProbability() {
		return crossoverProbability;
	}

	public boolean crossover(List<Chromosome> population, Chromosome chromosomeA, Chromosome chromosomeB) throws Exception {
		int originalIndexA = population.indexOf(chromosomeA);
		int originalIndexB = population.indexOf(chromosomeB);
		Chromosome testCrossoverA = new Chromosome(chromosomeA);
		Chromosome testCrossoverB = new Chromosome(chromosomeB);
		Random generator = new Random();
		if (chromosomeA.size() > 1 && chromosomeB.size() > 1) {
			int crossoverIndexA = generator.nextInt(chromosomeA.size() - 1) + 1;
			int crossoverIndexB = generator.nextInt(chromosomeB.size() - 1) + 1;
			if (crossoverIndexA + testCrossoverB.size() - crossoverIndexB < 5 && crossoverIndexB + testCrossoverA.size() - crossoverIndexA < 5) {
				Chromosome buildNewChromosomeA = buildNewChromosome(testCrossoverA, testCrossoverB, crossoverIndexA, crossoverIndexB);
				Chromosome buildNewChromosomeB = buildNewChromosome(testCrossoverB, testCrossoverA, crossoverIndexB, crossoverIndexA);
				double originalFitness = chromosomeA.getFitness() + chromosomeB.getFitness();
				double crossoverFitness = buildNewChromosomeA.getFitness() + buildNewChromosomeB.getFitness();
				if (crossoverFitness > originalFitness) {
					population.set(originalIndexA, buildNewChromosomeA);
					population.set(originalIndexB, buildNewChromosomeB);
					return true;
				}
			}
		}
		return false;
	}

	private Chromosome buildNewChromosome(Chromosome testCrossoverA, Chromosome testCrossoverB, int crossoverIndexA, int crossoverIndexB) throws Exception {
		Chromosome chromosome = new Chromosome();
		for (int i = 0; i < crossoverIndexA + testCrossoverB.size() - crossoverIndexB; i++) {
			if (i < crossoverIndexA) {
				chromosome.addGene(testCrossoverA.getGeneAt(i));
			} else {
				chromosome.addGene(testCrossoverB.getGeneAt(i - crossoverIndexA + crossoverIndexB));
			}
		}
		return chromosome;
	}
}
