package geneticalgorithm;


import client.Preferences;
import client.Ride;

import java.util.ArrayList;
import java.util.List;

public class ChromosomeHelper {

	public static List<List<Ride>> splitByDestinations(Chromosome chromosome) throws Exception {
		List<List<Ride>> chromosomes = new ArrayList<>();
		List<Ride> genes = chromosome.getGenes();
		for (Ride user : genes) {
			if (user.isDriver()) {
				List<Ride> newList = new ArrayList<>();
				newList.add(user);
				chromosomes.add(newList);
			}
		}
		for (Ride user : genes) {
			if (!user.isDriver()) {
				if (!findMatch(user, chromosomes)) {
					List<Ride> newList = new ArrayList<>();
					newList.add(user);
					chromosomes.add(newList);
				}
			}
		}
		group(chromosomes);
		return chromosomes;
	}

	public static ArrayList<Integer> preferencesUnmet(Chromosome chromosome) {
		/**
		 * 0 - SMOKING, 1 - AGE GROUP, 2 - CAR CAPACITY
		 */
		ArrayList<Integer> unmet = new ArrayList<>();
		unmet.add(0);
		unmet.add(0);
		unmet.add(0);
		List<Ride> genes = chromosome.getGenes();
		for (int i = 0; i < genes.size(); i++) {
			for (int j = 0; j < genes.size(); j++) {
				if (i != j) {
					Preferences preferences = genes.get(i).getUser().getPreferences();
					if (preferences.isSmokingCondition()) {
						if (genes.get(j).getUser().getPreferences().isSmokes()) {
							unmet.set(0, unmet.get(0) + 1);
						}
					}
					if (preferences.isAgeGroupCondition()) {
						if (preferences.getAgeGroup() != genes.get(j).getUser().getPreferences().getAgeGroup()) {
							unmet.set(1, unmet.get(1) + 1);
						}
					}
				}
			}
		}
		if (chromosome.size() > chromosome.getDriver().getUser().getPreferences().getPreferredCarCapacity()) {
			unmet.set(2, 1);
		}
		return unmet;
	}

	private static void group(List<List<Ride>> chromosomes) throws Exception {
		for (int i = 0; i < chromosomes.size(); i++) {
			if (chromosomes.get(i).size() == 1) {
				findMatch(chromosomes.get(i).get(0), chromosomes);
			}
		}
	}

	private static boolean findMatch(Ride user, List<List<Ride>> chromosomes) throws Exception {
		int index = -1;
		double min = chromosomes.get(0).get(0).getOrigin().distance(user.getOrigin())
				+ chromosomes.get(0).get(0).getDestination().distance(user.getDestination());
		for (List<Ride> ride : chromosomes) {
			if (ride.get(0).getDestination().equals(user.getDestination())) {
				double minOrigin = ride.get(0).getOrigin().distance(user.getOrigin());
				double minDestination = ride.get(0).getDestination().distance(user.getDestination());
				if (min > minOrigin + minDestination && !ride.contains(user)) {
					min = minOrigin + minDestination;
					index = chromosomes.indexOf(ride);
				}
			}
		}
		if (index != -1) {
			for (int i = 0; i < chromosomes.size(); i++) {
				if (chromosomes.get(i).contains(user)) {
					chromosomes.get(i).remove(user);
					if (chromosomes.get(i).isEmpty()) {
						chromosomes.remove(chromosomes.get(i));
						if (index > i) {
							index--;
						}
					}
				}
			}
			chromosomes.get(index).add(user);
			return true;
		} else {
			return false;
		}
	}
}