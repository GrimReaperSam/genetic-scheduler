package geneticalgorithm;


import client.Coordinates;
import client.Ride;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class PopulationHelper {
	private static Crossover crossover = new Crossover();
	private static Mutation mutation = new Mutation();
	private static DriverSwitch driverSwitch = new DriverSwitch();

	public static void evolve(List<Chromosome> population) throws Exception {
		for (int i = 0; i < population.size(); i++) {
			for (int j = 0; j < population.size(); j++) {
				if (new Random().nextDouble() < crossover.getCrossoverProbability() && i != j) {
					crossover.crossover(population, population.get(i), population.get(j));
				}
			}
		}
		for (int i = 0; i < population.size(); i++) {
			for (int j = 0; j < population.get(i).size(); j++) {
				if (new Random().nextDouble() < mutation.getMutationProbability()) {
					mutation.mutate(population, population.get(i), j);
				}
			}
		}
		for (int i = 0; i < population.size(); i++) {
			if (new Random().nextDouble() < driverSwitch.getDriverSwitchProbability()) {
				driverSwitch.driverSwitch(population, population.get(i));
			}
		}
	}

	public static int getMinimumFitness(List<Chromosome> population) throws Exception {
		double minFitness = population.get(0).getFitness();
		int worstIndex = 0;
		for (Chromosome chromosome : population) {
			double fitness = chromosome.getFitness();
			if (fitness < minFitness) {
				minFitness = fitness;
				worstIndex = population.indexOf(chromosome);
			}
		}
		return worstIndex;
	}

	public static void solveWorstCase(List<Chromosome> population) throws Exception {
		int worstIndex = getMinimumFitness(population);
		Chromosome chromosome = population.get(worstIndex);
		population.remove(chromosome);
		if (chromosome.size() > 1) {
			List<List<Ride>> splitByDestinations = ChromosomeHelper.splitByDestinations(chromosome);
			for (List<Ride> list : splitByDestinations) {
				int findDriver = findDriver(list);
				if (findDriver != -1) {
					if (list.size() == 1) {
						// int findMatch = findMatch(population, list.get(0));
						// if (findMatch != -1) {
						// population.get(findMatch).addGene(list.get(0));
						// } else {
						List<Ride> driver = new ArrayList<>();
						driver.add(list.get(0));
						Chromosome newOne = new Chromosome(driver);
						population.add(newOne);
					} else {
						Collections.swap(list, findDriver, 0);
						Chromosome newOne = new Chromosome(list);
						population.add(newOne);
					}
				} else {
					for (Ride rider : list) {
						int findMatch = findMatch(population, rider);
						if (findMatch != -2) {
							population.get(findMatch).addGene(rider);
						}
					}
				}
			}
		} else {
			int findMatch = findMatch(population, chromosome.getDriver());
			if (findMatch != -1) {
				population.get(findMatch).addGene(chromosome.getDriver());
			} else {
				population.add(chromosome);
			}
		}
	}

	//
	// private static int size(List<Chromosome> population) {
	// int count = 0;
	// for (Chromosome c : population) {
	// count += c.size();
	// }
	// return count;
	// }

	private static int findMatch(List<Chromosome> population, Ride rider) throws Exception {
		int index = 0;
        Ride driver = population.get(0).getDriver();
        double originValue = driver.getOrigin().distance(rider.getOrigin());
        double destinationValue = driver.getDestination().distance(rider.getDestination());
        double min = originValue + destinationValue;
		for (int i = 1; i < population.size(); i++) {
            driver = population.get(i).getDriver();
			originValue = driver.getOrigin().distance(rider.getOrigin());
            destinationValue = driver.getDestination().distance(rider.getDestination());
            if (destinationValue + originValue < min) {
				if (population.get(i).size() < 6) {
					System.out.println("This " + population.get(i).size());
					min = destinationValue + originValue;
					index = i;
				}
			}
		}
		System.out.println("X: " + index + "    " + population.get(index).size());
		if (population.get(index).size() < 6) {
			return index;
		} else {
			if (rider.isDriver()) {
				return -1;
			} else {
				return createNewRide(population, rider);
			}
		}
	}

	private static int createNewRide(List<Chromosome> population, Ride rider) throws Exception {
		List<Ride> PD = new ArrayList<>();
		for (Chromosome chromosome : population) {
			List<Ride> potentialDrivers = chromosome.getPotentialDrivers();
			if (potentialDrivers.size() > 1) {
                PD = new ArrayList<>(potentialDrivers.subList(1, potentialDrivers.size() - 1));
			}
		}
		double min = PD.get(0).getOrigin().distance(rider.getOrigin())
				+ PD.get(0).getDestination().distance(rider.getDestination());
		Ride bestDriver = PD.get(0);
		for (Ride driver : PD) {
			double destinationValue = driver.getDestination().distance(rider.getDestination());
			double originValue = driver.getOrigin().distance(rider.getOrigin());
			if (destinationValue + originValue < min) {
				min = destinationValue + originValue;
				bestDriver = driver;
			}
		}
		List<Ride> newList = new ArrayList<>();
		newList.add(bestDriver);
		Chromosome newOne = new Chromosome(newList);
		population.add(newOne);
		return population.size() - 1;
	}

	// TODO implement into findBestDriver
	private static int findDriver(List<Ride> list) {
		for (Ride user : list) {
			if (user.isDriver()) {
				return list.indexOf(user);
			}
		}
		return -1;
	}

	public static void printStatistics(List<Chromosome> population, PrintWriter out) throws Exception {
		for (Chromosome chromosome : population) {
			List<Coordinates> path = chromosome.getPath();
			out.println(path + "\n");
		}
	}

}
