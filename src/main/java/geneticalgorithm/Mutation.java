package geneticalgorithm;

import java.util.List;
import java.util.Random;

public class Mutation {
	private static final double INITIAL_MUTATION_PROBABILITY = 0.5;

	private double mutationProbability;

	public Mutation() {
		this.setMutationProbability(INITIAL_MUTATION_PROBABILITY);
	}

	public void setMutationProbability(double mutationProbability) {
		this.mutationProbability = mutationProbability;
	}

	public double getMutationProbability() {
		return mutationProbability;
	}

	public boolean mutate(List<Chromosome> population, Chromosome chromosome, int mutationIndex) throws Exception {
		Chromosome mutated = new Chromosome(chromosome);
		int originalIndex = population.indexOf(chromosome);
		Random generator = new Random();
		int chosenChromosomeIndex = generator.nextInt(population.size());
		while (population.indexOf(chromosome) == chosenChromosomeIndex) {
			chosenChromosomeIndex = generator.nextInt(population.size());
		}
		Chromosome chosenChromosome = new Chromosome(population.get(chosenChromosomeIndex));
		int chosenGeneIndex = generator.nextInt(chosenChromosome.size());
		// Check if it is a driver
		if (mutationIndex == 0) {
			if (chosenChromosome.getGeneAt(chosenGeneIndex).isDriver()) {
				mutated.swap(chosenChromosome, chosenChromosome.getGeneAt(chosenGeneIndex), mutated.getGeneAt(mutationIndex));
			}
		} else {
			if (chosenGeneIndex == 0) {
				if (mutated.getGeneAt(mutationIndex).isDriver()) {
					mutated.swap(chosenChromosome, chosenChromosome.getDriver(), mutated.getGeneAt(mutationIndex));
				}
			} else {
				mutated.swap(chosenChromosome, chosenChromosome.getGeneAt(chosenGeneIndex), mutated.getGeneAt(mutationIndex));
			}
		}
		// Check if new chromosome is fitter than older version
		double originalFitness = chromosome.getFitness() + population.get(chosenChromosomeIndex).getFitness();
		double mutationFitness = mutated.getFitness() + chosenChromosome.getFitness();
		if (mutationFitness > originalFitness) {
			population.set(originalIndex, mutated);
			population.set(chosenChromosomeIndex, chosenChromosome);
			return true;
		}
		return false;
	}
}
