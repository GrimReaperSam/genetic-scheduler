package initialsolution;

import client.Ride;
import client.RideBuilder;
import geneticalgorithm.Chromosome;
import threadedgeneticalgorithm.GaThread;

import java.util.ArrayList;
import java.util.List;

public class InitialSolutions {

	public InitialSolutions() {
	}

	// TODO corect implementation of forLoop when done with testing
	public List<List<Chromosome>> getInitialSolutions(List<Ride> potentialDrivers, List<Ride> strictlyPassengers) {
		List<List<Chromosome>> solutions = new ArrayList<>();
		int counter = 0;
		for (int i = getMinDrivers(potentialDrivers, strictlyPassengers); i <= getMinDrivers(potentialDrivers, strictlyPassengers); i++) {
			counter++;
			new GaThread(copyOf(potentialDrivers), copyOf(strictlyPassengers), i + 5 + counter * 2).start();
			// InitialSolution initialSolution = new InitialSolution();
			// solutions.add(initialSolution.getInitialSolution(copyOf(potentialDrivers),
			// copyOf(strictlyPassengers), i));
		}
		return solutions;
	}

	private List<Ride> copyOf(List<Ride> listOfUsers) {
		List<Ride> copyList = new ArrayList<>();
		for (int i = 0; i < listOfUsers.size(); i++) {
			Ride temp = listOfUsers.get(i);
			copyList.add(new RideBuilder(temp.getUser()).origin(temp.getOrigin()).destination(temp.getDestination()).arrival(temp.getArrival()).departure(temp.getDeparture()).capacity(temp
                    .getCarCapacity()).createRide());
		}
		return copyList;
	}

	private int getMinDrivers(List<Ride> potentialDrivers, List<Ride> strictlyPassengers) {
		Mergesort sorter = new Mergesort();
		sorter.sort(potentialDrivers);
		int remainingUsers = potentialDrivers.size() + strictlyPassengers.size();
		for (int i = 0; i < potentialDrivers.size(); i++) {
			remainingUsers = remainingUsers - potentialDrivers.get(i).getCarCapacity() - 1;
			if (remainingUsers <= 0) {
				return i + 1;
			}
		}
		throw new IllegalAccessError("ERROR IN MINDRIVERS CALCULATION DO NOT PANIC !");
	}

}
