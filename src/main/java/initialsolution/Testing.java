 package initialsolution;

 import client.Coordinates;
 import client.Ride;
 import client.User;
 import geneticalgorithm.Chromosome;
 import org.joda.time.DateTime;
 import org.joda.time.Interval;

 import java.io.FileWriter;
 import java.io.PrintWriter;
 import java.util.ArrayList;
 import java.util.List;
 import java.util.Random;

 public class Testing {
 	public static void main(String[] args) throws Exception {
 		testPathOptimizer();
 		testMergeSort();
 		testInitialSolution();
 	}
 
   private static void testPathOptimizer() throws Exception {/*
 	  Ride driver = new Ride(new Coordinates(0, 0), new Coordinates(0, 100));
 	  Ride passenger1 = new Ride(new Coordinates(0, 140), new Coordinates(0,
 	  50));
 	  Ride passenger2 = new Ride(new Coordinates(0, 80), new Coordinates(0,
 	  60));
 	  Ride passenger3 = new Ride(new Coordinates(0, 110), new Coordinates(0,
 	  150));
 	  Ride passenger4 = new Ride(new Coordinates(0, 110), new Coordinates(0,
 	  90));
 	  List<Ride> ride = new ArrayList<Ride>();
 	  ride.add(driver);
 	  ride.add(passenger1);
 	  ride.add(passenger2);
 	  ride.add(passenger3);
 	  ride.add(passenger4);
 	  Chromosome chromosomeRide = new Chromosome(ride);
 	  long startTime = System.currentTimeMillis();
 	  PathOptimizerDP optimizer = new PathOptimizerDP();
 	  List<Coordinates> bestPath = optimizer.getPath(chromosomeRide);
 	  long endTime = System.currentTimeMillis();
 	  System.out.println("That took " + (endTime - startTime) +
 	  " milliseconds");
 	  for (int i = 0; i < bestPath.size(); i++) {
 	  System.out.println("(" + bestPath.get(i).getLatitude() + "," +
 	  bestPath.get(i).getLongitude() + ")");
 	  }
 	 
 	  startTime = System.currentTimeMillis();
 	  PathOptimizerDP optimizerDP = new PathOptimizerDP();
 	  optimizerDP.getPathCost(chromosomeRide);
 	  endTime = System.currentTimeMillis();
 	  System.out.println("That took " + (endTime - startTime) +
 	  " milliseconds");
 	  startTime = System.currentTimeMillis();
 	  bestPath = optimizerDP.getPath(chromosomeRide);
 	  endTime = System.currentTimeMillis();
 	  System.out.println("That took " + (endTime - startTime) +
 	  " milliseconds");
 	  for (int i = 0; i < bestPath.size(); i++) {
 	  System.out.println("(" + bestPath.get(i).getLatitude() + "," +
 	  bestPath.get(i).getLongitude() + ")");
 	  }*/
 	  }
 
 	private static void testInitialSolution() throws Exception {
 		int drivers = 40;
 		int passengers = 60;
 		int destinations = 25;
 		List<Ride> PD = new ArrayList<>();
 		List<Ride> SR = new ArrayList<>();
 		List<Coordinates> destination = new ArrayList<>();
 
 		for (int i = 0; i < destinations; i++) {
 			destination.add(randomCoordinates());
 		}
 
 		for (int i = 0; i < drivers; i++) {
 			Interval[] timeWindows = getRandomTimeWindows();
 			User user = new User(true);
            Ride ride = user.newRide().origin(randomCoordinates()).destination(destination.get(new Random().nextInt(25))).departure(timeWindows[0]).arrival(timeWindows[1]).createRide();
			PD.add(ride);
 		}
 
 		for (int j = 0; j < passengers; j++) {
            Interval[] timeWindows = getRandomTimeWindows();
            User user = new User(true);
            Ride ride = user.newRide().origin(randomCoordinates()).destination(destination.get(new Random().nextInt(25))).departure(timeWindows[0]).arrival(timeWindows[1]).createRide();
            PD.add(ride);
 		}
 		InitialSolutions solver = new InitialSolutions();
 		List<List<Chromosome>> solution = solver.getInitialSolutions(PD, SR);
 		
 
 		PrintWriter out = new PrintWriter(new FileWriter("outputfile.txt"));
 		for (int i = 0; i < solution.size(); i++) {
 			out.println("Solution: " + i);
 			for (int y = 0; y < solution.get(i).size(); y++) {
 				Chromosome combo = solution.get(i).get(y);
 				out.println("\nRide " + y + " :");
 				for (int z = 0; z < combo.size(); z++) {
 					Ride rider = combo.getGeneAt(z);
 					if (z == 0)
 						out.println("Driver: Origin: ("
 								+ rider.getOrigin().getLatitude()
 								+ ","
 								+ rider.getOrigin().getLongitude()
 								+ ") Destination: ("
 								+ rider.getDestination()
 										.getLatitude()
 								+ ","
 								+ rider.getDestination()
 										.getLongitude() + ")"
 								+" TimeWindow: Start :  "
 								+rider.getArrival().getStart().getHourOfDay()+":"+rider.getArrival().getStart().getMinuteOfHour()
 								+" End :  "
 								+rider.getArrival().getEnd().getHourOfDay()+":"+rider.getArrival().getEnd().getMinuteOfHour()
 								);
 					else
 						out.println("Passenger "
 								+ z
 								+ ": Origin: ("
 								+ rider.getOrigin().getLatitude()
 								+ ","
 								+ rider.getOrigin().getLongitude()
 								+ ") Destination: ("
 								+ rider.getDestination()
 										.getLatitude()
 								+ ","
 								+ rider.getDestination()
 											.getLongitude() + ")"+" TimeWindow: Start :  "
 		 						+rider.getArrival().getStart().getHourOfDay()+":"+rider.getArrival().getStart().getMinuteOfHour()
 		 						+" End :  "
 		 						+rider.getArrival().getEnd().getHourOfDay()+":"+rider.getArrival().getEnd().getMinuteOfHour()
 		 						);
 				}
 			}
 			out.println("\n \n \n");
 		}
 		out.close();
 	}
 	
 	//private List<TimeWindow> getRandomTimeWindows 
 
 	private static void testMergeSort() {/*
 		int drivers = 40;
 		int destinations = 20;
 		List<Ride> PD = new ArrayList<Ride>();
 		List<Coordinates> destination = new ArrayList<Coordinates>();
 
 		for (int i = 0; i < destinations; i++) {
 			destination.add(randomCoordinates());
 		}
 
 		for (int i = 0; i < drivers; i++) {
 			PD.add(new Ride(randomCoordinates(), destination.get(new Random()
 					.nextInt(destination.size())), true, new Random()
 					.nextInt(5)));
 		}
 
 		Mergesort sorter = new Mergesort();
 		sorter.sort(PD);
 
 		for (int i = 0; i < PD.size(); i++) {
 			System.out.println(PD.get(i).getCarCapacity());
 		}*/
 	}
 
 	private static Interval[] getRandomTimeWindows(){
 		Interval[] temp = new Interval[2];
        DateTime a = new DateTime().withHourOfDay(new Random().nextInt(12)).withMinuteOfHour(new Random().nextInt(60));
        DateTime b = new DateTime().withHourOfDay(new Random().nextInt(24)).withMinuteOfHour(new Random().nextInt(60));
        DateTime c = new DateTime().withHourOfDay(a.getHourOfDay() + 1).withMinuteOfHour(new Random().nextInt(60));
        DateTime d = new DateTime().withHourOfDay(b.getHourOfDay() + 1).withMinuteOfHour(new Random().nextInt(60));
 		if(a.compareTo(b) >= 0){
 			temp[0] = new Interval(a,c);
            temp[1] = new Interval(b,d);
 		}
 		else {
            temp[0] = new Interval(b,d);
            temp[1] = new Interval(a,c);
 		}
 		return temp;
 	}
 	private static Coordinates randomCoordinates() {
 		return new Coordinates(new Random().nextInt(101) * (-1)
 				^ (new Random().nextInt(1)), new Random().nextInt(101) * (-1)
 				^ (new Random().nextInt(2)));
 	}
 
  }
