package initialsolution;

import client.Ride;
import geneticalgorithm.Chromosome;
import org.joda.time.Duration;
import org.joda.time.Interval;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class InitialSolution {
	public InitialSolution() {
	}// assume for now all lie in the time window

	public List<Chromosome> getInitialSolution(List<Ride> PD, List<Ride> SR, int numberOfDrivers) throws Exception {
		List<Chromosome> solution = new ArrayList<>();

		// creates and fills numberOfDrivers rides
		double distanceGlobalRatio;
		double timeGlobalRatio;

		for (int i = 0; i < numberOfDrivers; i++) {
			List<Ride> ride = new ArrayList<>();
			int driverIndex = getRandomDriver(PD);
			ride.add(0, PD.remove(driverIndex));
			solution.add(new Chromosome(ride));
		}

		// empties potentialDrivers in strictly passengers
		while (!PD.isEmpty()) {
			SR.add(PD.remove(0));
		}
		List<Ride> drivers = solution.stream().map(Chromosome::getDriver).collect(Collectors.toList());

		// find rides to the remaining passengers
		while (!SR.isEmpty()) {
			distanceGlobalRatio = getDistanceGlobalRatio(drivers, SR, false);
			timeGlobalRatio = getTimeGlobalRatio(drivers, SR, false);
			boolean add = false;
			for (int i = 0; i < solution.size(); i++) {
				Ride driver = solution.get(i).getDriver();
				for (int j = 0; j < SR.size(); j++) {

					if (driver.getArrival().overlaps(getNewTimeWindow(driver, SR.get(j))) && driver.getCarCapacity() > 0
							&& getDistanceRatio(driver, SR.get(j)) <= distanceGlobalRatio) {
						add = true;
						List<Ride> newGene = solution.get(i).getGenes();
						newGene.add(SR.get(j));
						newGene.get(0).decrementRemainingCarCapacity();
						solution.get(i).setGenes(newGene);
						SR.remove(j);
						j--;
					}
				}
			}

			if (!add) {
				for (int i = 0; i < solution.size(); i++) {
					Ride driver = solution.get(i).getDriver();
					for (int j = 0; j < SR.size(); j++) {

						if (driver.getArrival().overlaps(getNewTimeWindow(driver, SR.get(j))) && driver.getCarCapacity() > 0
								&& getDistanceRatio(driver, SR.get(j)) <= distanceGlobalRatio) {
							add = true;
							List<Ride> newGene = solution.get(i).getGenes();
							newGene.add(SR.get(j));
							newGene.get(0).decrementRemainingCarCapacity();
							solution.get(i).setGenes(newGene);
							SR.remove(j);
							j--;
						}
					}
				}
			}
			if (!add) {
				for (int i = 0; i < solution.size(); i++) {
					Ride driver = solution.get(i).getDriver();
					for (int j = 0; j < SR.size(); j++) {

						if (driver.getCarCapacity() > 0 && getDistanceRatio(driver, SR.get(j)) <= distanceGlobalRatio && getTimeDifference(driver, SR.get(j)) <= timeGlobalRatio) {
							add = true;
							List<Ride> newGene = solution.get(i).getGenes();
							newGene.add(SR.get(j));
							newGene.get(0).decrementRemainingCarCapacity();
							solution.get(i).setGenes(newGene);
							SR.remove(j);
							j--;
						}
					}
				}
			}
			if (!add) {
				for (int i = 0; i < solution.size(); i++) {
					Ride driver = solution.get(i).getDriver();
					for (int j = 0; j < SR.size(); j++) {

						if (driver.getCarCapacity() > 0 && getTimeDifference(driver, SR.get(j)) <= timeGlobalRatio) {
							List<Ride> newGene = solution.get(i).getGenes();
							newGene.add(SR.get(j));
							newGene.get(0).decrementRemainingCarCapacity();
							solution.get(i).setGenes(newGene);
							SR.remove(j);
							j--;
						}
					}
				}
			}
		}
		return solution;
	}

	private int getRandomDriver(List<Ride> PD) {
		return new Random().nextInt(PD.size());
	}

	private double getDistanceGlobalRatio(List<Ride> PD, List<Ride> SR, boolean beforePickingDriver) throws Exception {
		int count = 0;
		double sumOfRatios = 0;
		for (int i = 0; i < PD.size(); i++) {
			if (beforePickingDriver) {
				for (int j = 0; j < PD.size(); j++) {
					if (i != j) {
						sumOfRatios = sumOfRatios + getDistanceRatio(PD.get(i), PD.get(j));
						count++;
					}
				}
			} else {
				while (PD.get(i).getCarCapacity() == 0) {
					i++;
					if (i == PD.size()) {
						return sumOfRatios / count;
					}
				}
			}
			for (int j = 0; j < SR.size(); j++) {
				sumOfRatios = sumOfRatios + getDistanceRatio(PD.get(i), SR.get(j));
				count++;
			}
		}
		return sumOfRatios / count;

	}

	private double getTimeGlobalRatio(List<Ride> PD, List<Ride> SR, boolean beforePickingDriver) throws Exception {
		int count = 0;
		double sumOfRatios = 0;
		for (int i = 0; i < PD.size(); i++) {
			if (beforePickingDriver) {
				for (int j = 0; j < PD.size(); j++) {
					if (i != j) {
						sumOfRatios = sumOfRatios + getTimeDifference(PD.get(i), PD.get(j));
						count++;
					}
				}
			} else {
				while (PD.get(i).getCarCapacity() == 0) {
					i++;
					if (i == PD.size()) {
						return sumOfRatios / count;
					}
				}
			}
			for (int j = 0; j < SR.size(); j++) {
				sumOfRatios = sumOfRatios + getTimeDifference(PD.get(i), SR.get(j));
				count++;
			}
		}
		return sumOfRatios / count;

	}

	private double getDistanceRatio(Ride driver, Ride passenger) throws Exception {
		double originalDistance = driver.getOrigin().distance(driver.getDestination());
		double pickUpDistance = driver.getOrigin().distance(passenger.getOrigin())
				+ passenger.getOrigin().distance(passenger.getDestination())
				+ passenger.getDestination().distance(driver.getDestination());
		return (pickUpDistance - originalDistance) / originalDistance;
	}

	private Interval getNewTimeWindow(Ride driver, Ride passenger) throws Exception {
		double time = driver.getOrigin().duration(passenger.getOrigin());
        Duration duration = new Duration((long) time);
		Interval timeWindow = passenger.getArrival();
        return new Interval(timeWindow.getStart().plus(duration), timeWindow.getEnd().plus(duration));
	}

	private double getTimeDifference(Ride driver, Ride passenger) throws Exception {
        Interval gap = driver.getArrival().gap(getNewTimeWindow(driver, passenger));
        return gap == null ? 0 : gap.toDuration().getStandardHours();
	}

}
