package initialsolution;


import client.Ride;

import java.util.ArrayList;
import java.util.List;

public class Mergesort {
	private List<Ride> users;
	private List<Ride> helper;


	public void sort(List<Ride> values) {
		users = values;
		int number = values.size();
		helper = new ArrayList<>(number);
		mergesort(0, number - 1);
	}

	private void mergesort(int low, int high) {
		// Check if low is smaller then high, if not then the array is sorted
		if (low < high) {
			// Get the index of the element which is in the middle
			int middle = low + (high - low) / 2;
			// Sort the left side of the array
			mergesort(low, middle);
			// Sort the right side of the array
			mergesort(middle + 1, high);
			// Combine them both
			merge(low, middle, high);
		}
	}

	private void merge(int low, int middle, int high) {

		// Copy both parts into the helper array
		for (int i = low; i <= high; i++) {
			helper.add(i, users.get(i));
		}

		int i = low;
		int j = middle + 1;
		int k = low;
		// Copy the smallest values from either the left or the right side back
		// to the original array
		while (i <= middle && j <= high) {
			if (helper.get(i).getCarCapacity() > helper.get(j).getCarCapacity()) {
				users.set(k, helper.get(i));
				i++;
			} else {
				users.set(k, helper.get(j));
				j++;
			}
			k++;
		}
		// Copy the rest of the left side of the array into the target array
		while (i <= middle) {
			users.set(k, helper.get(i));
			k++;
			i++;
		}

	}
}
