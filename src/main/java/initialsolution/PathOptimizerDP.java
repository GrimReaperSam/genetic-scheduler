package initialsolution;

import client.Coordinates;
import client.Ride;
import geneticalgorithm.Chromosome;

import java.util.ArrayList;
import java.util.List;

public class PathOptimizerDP {

	private List<Coordinates> pickUpCoordinates = new ArrayList<>();
	private List<Coordinates> allCoordinates = new ArrayList<>();
	private double memoization[][][] = new double[64][64][12];
	private int indexArray[][][] = new int[64][64][12];
	private Ride driver;

	public PathOptimizerDP() {
	}

	public double getPathCost(Chromosome chromosome) throws Exception {
		driver = chromosome.getDriver();
		pickUpCoordinates.clear();
		allCoordinates.clear();
		if (chromosome.size() == 1) {
			return driver.getOrigin().distance(driver.getDestination());
		}

		for (int i = 0; i < chromosome.size() - 1; i++) {
			pickUpCoordinates.add(chromosome.getGeneAt(i + 1).getOrigin());
			allCoordinates.add(chromosome.getGeneAt(i + 1).getOrigin());
		}
		for (int i = 0; i < chromosome.size() - 1; i++) {
			allCoordinates.add(chromosome.getGeneAt(i + 1).getDestination());
		}
		for (int i = 0; i < 64; i++) {
			for (int j = 0; j < 64; j++) {
				for (int k = 0; k < 12; k++) {
					memoization[i][j][k] = -1;
				}
			}
		}
		return optimizePathDP(0, 0, 0);
	}

	// u have to run optimizePath first
	public List<Coordinates> getPath(Chromosome ride) throws Exception {
		driver = ride.getDriver();
		pickUpCoordinates.clear();
		allCoordinates.clear();
		List<Coordinates> path = new ArrayList<>();
		if (ride.size() == 1) {
			path.add(driver.getOrigin());
			path.add(driver.getDestination());
			return path;
		}

		for (int i = 0; i < ride.size() - 1; i++) {
			pickUpCoordinates.add(ride.getGeneAt(i + 1).getOrigin());
			allCoordinates.add(ride.getGeneAt(i + 1).getOrigin());
		}
		for (int i = 0; i < ride.size() - 1; i++) {
			allCoordinates.add(ride.getGeneAt(i + 1).getDestination());
		}
		for (int i = 0; i < 64; i++) {
			for (int j = 0; j < 64; j++) {
				for (int k = 0; k < 12; k++) {
					memoization[i][j][k] = -1;
				}
			}
		}
		optimizePathDP(0, 0, 0);
		int index = indexArray[0][0][0];
		int pickedUp = 0;
		int droppedOff = 0;
		path.add(driver.getOrigin());
		while (index != -1) {
			path.add(allCoordinates.get(index));

			if (index < pickUpCoordinates.size()) {
				pickedUp = pickedUp | 1 << index;
			} else {
				droppedOff = droppedOff | 1 << (index - pickUpCoordinates.size());
			}
			index = indexArray[pickedUp][droppedOff][index];
		}
		path.add(driver.getDestination());

		return removeDuplicateCoordinates(path);
	}

	private List<Coordinates> removeDuplicateCoordinates(List<Coordinates> path) {
		List<Coordinates> temp = new ArrayList<>();
		for (int i = 0; i < path.size() - 1; i++) {
			if ((path.get(i).getLatitude() != path.get(i + 1).getLatitude()) || (path.get(i).getLongitude() != path.get(i + 1).getLongitude())) {
				temp.add(path.get(i));
			}
		}
		temp.add(path.get(path.size() - 1));
		return temp;
	}

	private double optimizePathDP(int pickedUp, int droppedOff, int index) throws Exception {
		if (memoization[pickedUp][droppedOff][index] != -1) {
			return memoization[pickedUp][droppedOff][index];
		}
		if (isFull(pickedUp) && isFull(droppedOff)) {
			indexArray[pickedUp][droppedOff][index] = -1;
			return memoization[pickedUp][droppedOff][index] = allCoordinates.get(index).distance(driver.getDestination());
		}
		double min = Double.MAX_VALUE;
		Coordinates currentCoordinate;
		if (index == 0 && pickedUp == 0) {
			currentCoordinate = driver.getOrigin();
		} else {
			currentCoordinate = allCoordinates.get(index);
		}
		for (int i = 0; i < pickUpCoordinates.size(); i++) {
			double temp;
			if ((pickedUp & 1 << i) == 0) {
				temp = optimizePathDP(pickedUp | 1 << i, droppedOff, i) + currentCoordinate.distance(allCoordinates.get(i));
				if (temp < min) {
					min = temp;
					indexArray[pickedUp][droppedOff][index] = i;
				}
			} else if ((droppedOff & 1 << i) == 0) {
				temp = optimizePathDP(pickedUp, droppedOff | 1 << i, i + pickUpCoordinates.size()) + currentCoordinate.distance(allCoordinates.get(i + pickUpCoordinates.size()));
				if (temp < min) {
					min = temp;
					indexArray[pickedUp][droppedOff][index] = i + pickUpCoordinates.size();
				}
			}
		}

		return memoization[pickedUp][droppedOff][index] = min;

	}

	private boolean isFull(int bitSet) {
		for (int i = 0; i < pickUpCoordinates.size(); i++) {
			if ((bitSet & 1 << i) == 0) {
				return false;
			}
		}
		return true;
	}

}
