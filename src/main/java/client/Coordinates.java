package client;

public class Coordinates {

	private Double latitude;
	private Double longitude;

	public Coordinates(double x, double y) {
		latitude = x;
		longitude = y;
	}

	public Coordinates() {
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof Coordinates) {
			Coordinates coordinates = (Coordinates) object;
			return (coordinates.latitude.toString().equals(latitude.toString())) && (coordinates.longitude.toString().equals(longitude.toString()));
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "(" + this.latitude + "," + this.longitude + ")";
	}

	public double distance(Coordinates destination) throws Exception {
		double d2r = (Math.PI / 180);

		double destinationLongitude = (destination.getLongitude() - longitude) * d2r;
		double destinationLatitude = (destination.getLatitude() - latitude) * d2r;
		double af = Math.pow(Math.sin(destinationLatitude / 2.0), 2) + Math.cos(latitude * d2r) * Math.cos(destination.getLatitude() * d2r) * Math.pow(Math.sin(destinationLongitude / 2.0), 2);
		double c = 2 * Math.atan2(Math.sqrt(af), Math.sqrt(1 - af));
        return 6367 * c;
		// return GoogleMapsInfo.getDistance(origin, destination);
	}

	public double duration(Coordinates destination) throws Exception {
		return distance(destination);
		// return GoogleMapsInfo.getDuration(origin, destination);
	}

}
