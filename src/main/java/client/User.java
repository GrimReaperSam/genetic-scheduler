package client;

import java.util.ArrayList;
import java.util.List;

public class User {

	private boolean hasCar;
	private int fairness;
	private Preferences preferences;
	private List<Ride> rides;

	public User(boolean hasCar) {
		this.hasCar(hasCar);
		fairness = 0;
		preferences = new Preferences();
		rides = new ArrayList<>();
	}

	public boolean hasCar() {
		return hasCar;
	}

	public void hasCar(boolean hasCar) {
		this.hasCar = hasCar;
	}

	public void setFairness(int fairness) {
		this.fairness = fairness;
	}

	public int getFairness() {
		return fairness;
	}

    public RideBuilder newRide() {
        return new RideBuilder(this);
    }

	// TODO implement multiple rides perday
	public List<Ride> getRides() {
		return rides;
	}

	public Preferences getPreferences() {
		return preferences;
	}

	public void setPreferences(Preferences preferences) {
		this.preferences = preferences;
	}
}
