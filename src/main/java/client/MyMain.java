package client;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

public class MyMain {

    public static void main(String[] args) {

        DateTime a = new DateTime().withHourOfDay(12).withMinuteOfHour(16);
        DateTime b = new DateTime().withHourOfDay(15).withMinuteOfHour(25);
        Interval i = new Interval(a,b);
        System.out.println(i);
        Interval i2 = new Interval(i.getStart().plus(new Duration(60000)), i.getEnd().plus(new Duration(60000)));
        System.out.println(i2);
    }
}
