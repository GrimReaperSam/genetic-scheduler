package client;

public class Preferences {

	private boolean smokes;
	private boolean smokingCondition;
	private int smokingPriority;
	/**
	 * {18-25 -> 0; 25+ -> 1}
	 */
	private int ageGroup;
	private boolean ageGroupCondition;
	private int ageGroupPriority;
	private int preferredCarCapacity;
	private int carCapacityPriority;

	/**
	 * Higher priority means higher cost.
	 */
	public Preferences() {
		setSmokes(false);
		smokingCondition = false;
		smokingPriority = 3;
		ageGroup = 0;
		ageGroupCondition = false;
		ageGroupPriority = 1;
		preferredCarCapacity = 4;
		carCapacityPriority = 2;
	}

	public boolean isSmokingCondition() {
		return smokingCondition;
	}

	public void setSmokingCondition(boolean smoking) {
		this.smokingCondition = smoking;
	}

	public int getSmokingPriority() {
		return smokingPriority;
	}

	public void setSmokingPriority(int smokingPriority) {
		this.smokingPriority = smokingPriority;
	}

	public int getAgeGroup() {
		return ageGroup;
	}

	public void setAgeGroup(int ageGroup) {
		this.ageGroup = ageGroup;
	}

	public boolean isAgeGroupCondition() {
		return ageGroupCondition;
	}

	public void setAgeGroupCondition(boolean ageGroupCondition) {
		this.ageGroupCondition = ageGroupCondition;
	}

	public int getAgeGroupPriority() {
		return ageGroupPriority;
	}

	public void setAgeGroupPriority(int ageGroupPriority) {
		this.ageGroupPriority = ageGroupPriority;
	}

	public int getPreferredCarCapacity() {
		return preferredCarCapacity;
	}

	public void setPreferredCarCapacity(int preferredCarCapacity) {
		this.preferredCarCapacity = preferredCarCapacity;
	}

	public int getCarCapacityPriority() {
		return carCapacityPriority;
	}

	public void setCarCapacityPriority(int carCapacityPriority) {
		this.carCapacityPriority = carCapacityPriority;
	}

	public boolean isSmokes() {
		return smokes;
	}

	public void setSmokes(boolean smokes) {
		this.smokes = smokes;
	}

}
