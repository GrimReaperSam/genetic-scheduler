package client;

import java.sql.*;

public class DatabaseHelper {
	public static Statement statement;
	public static Connection connection;
	public static ResultSet resultSet;

	public static boolean checkIfUsernameExists(String tableName, String username) throws Exception {
		connect();
		String sql = String.format("SELECT * FROM " + tableName + " WHERE username = '" + username + "'");
		resultSet = statement.executeQuery(sql);
		if (resultSet.next()) {
			disconnect();
			return true;
		} else {
			disconnect();
			return false;
		}
	}

	public static void insertIntoDatabase(String tableName, String... fields) {
		try {
			connect();
			String sql = String.format("INSERT INTO " + tableName + " VALUES" + " (" + inSQl(fields) + ")");
			statement.executeUpdate(sql);
			disconnect();
		} catch (Exception e) {
		}
	}

	public static String getFromDatabase(String tableName, String username, String columnName) {
		try {
			connect();
			String value;
			String sql = String.format("SELECT " + columnName + " FROM " + tableName + " WHERE username = '" + username + "'");
			resultSet = statement.executeQuery(sql);
			if (resultSet.next()) {
				value = resultSet.getObject(1).toString();
			} else
				value = "";
			disconnect();
			return value;
		} catch (Exception e) {
			return null;
		}

	}

	// public static void up

	private static String inSQl(String... fields) {
		String result = "";
		for (String field : fields) {
			result = result + "'" + field + "', ";
		}
		return result.substring(0, result.length() - 2);
	}

	public static void main(String[] args) throws Exception {
		// SELECT * FROM login WHERE username = 'root'
		// insertIntoDatabase("login", "test","test");
		// updateDatabase("login","root","password", "roots");
		// System.out.println(getFromDatabase("login", "root", "password"));
		// insertIntoDatabase("rides","test","6.44","4.44","4.44","4.44","10:10:10.0","10:10:10.0","10:10:10.0","10:10:10.0","MWF");
		// deleteRide("test","4.44","4.44","4.44","4.44","10:10:10.0","10:10:10.0","10:10:10.0","10:10:10.0","MWF");
		/*
		 * Ride_Info rideinfo = new Ride_Info(new Coordinates(2.22,3.33), new
		 * Coordinates(4.44,5.55), new TimeWindow(new Date(6, 6), new Date(7, 7)), new
		 * TimeWindow(new Date(8, 8), new Date(9, 9)), "TWR"); newRide("test", rideinfo);
		 * System.out.println(getRides("test").get(2).getDaysOfTheWeek());
		 */
		// System.out.println(checkIfUsernameExists("login","tayaya"));

	}

	private static void connect() throws ClassNotFoundException, SQLException {
		Class.forName("org.sqlite.JDBC");
		connection = DriverManager.getConnection("jdbc:sqlite:C:/Users/fayezlahoud/Dropbox/Carpooling FYP/indexedfyp.db");
		statement = connection.createStatement();
	}

	private static void disconnect() throws ClassNotFoundException, SQLException {
		statement.close();
		connection.close();
	}
}
