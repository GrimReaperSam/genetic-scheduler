package client;

import org.joda.time.Interval;

public class RideBuilder {
    private Coordinates origin;
    private Coordinates destination;
    private User user;
    private int capacity = 4;
    private Interval arrival;
    private Interval departure;

    public RideBuilder(User user) {
        this.user = user;
    }

    public RideBuilder origin(Coordinates origin) {
        this.origin = origin;
        return this;
    }

    public RideBuilder destination(Coordinates destination) {
        this.destination = destination;
        return this;
    }

    public RideBuilder capacity(int capacity) {
        this.capacity = capacity;
        return this;
    }

    public RideBuilder arrival(Interval arrival) {
        this.arrival = arrival;
        return this;
    }

    public RideBuilder departure(Interval departure) {
        this.departure = departure;
        return this;
    }

    public Ride createRide() {
        Ride ride = new Ride();
        ride.setUser(user);
        ride.setOrigin(origin);
        ride.setDestination(destination);
        ride.setCarCapacity(capacity);
        ride.setDeparture(departure);
        ride.setArrival(arrival);
        user.getRides().add(ride);
        return ride;
    }
}