package client;

import org.joda.time.Interval;

import java.io.PrintWriter;

// TODO implement multiple rides perday
public class Ride {

	private Coordinates origin;
	private Coordinates destination;
	private int carCapacity;
	private Interval departure;
	private Interval arrival;
	private User user;

	protected Ride() {
    }

	public void setOrigin(Coordinates origin) {
		this.origin = origin;
	}

	public Coordinates getOrigin() {
		return origin;
	}

	public void setDestination(Coordinates destination) {
		this.destination = destination;
	}

	public Coordinates getDestination() {
		return destination;
	}

	public void setDeparture(Interval departure) {
		this.departure = departure;
	}

	public Interval getDeparture() {
		return departure;
	}

	public void setArrival(Interval arrival) {
		this.arrival = arrival;
	}

	public Interval getArrival() {
		return arrival;
	}

	public void setCarCapacity(int carCapacity) {
		this.carCapacity = carCapacity;
	}

	public int getCarCapacity() {
		return carCapacity;
	}

	public boolean isDriver() {
		return user.hasCar();
	}

	public void decrementRemainingCarCapacity() {
		if (getCarCapacity() == 0) {
            throw new IllegalArgumentException("No more capacity in car");
        }
		setCarCapacity(getCarCapacity() - 1);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

    public void print(PrintWriter out) {
        out.println("Origin: (" + this.getOrigin().getLatitude() + "," + this.getOrigin().getLongitude() + ")");
        out.println("Destination: (" + this.getDestination().getLatitude() + "," + this.getDestination().getLongitude() + ")");
        out.println("ArrivalWindow: " + this.getArrival());
        out.println("isDriver: " + this.isDriver());
        // TODO
//        if (isDriver()) {
//		}
    }
}
