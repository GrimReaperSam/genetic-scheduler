package threadedgeneticalgorithm;

import client.*;
import initialsolution.InitialSolutions;
import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ThreadTesting {

	public static void main(String[] args) throws Exception {
		List<Ride> PD = new ArrayList<>();
		List<Ride> SR = new ArrayList<>();
        URL resource = ThreadTesting.class.getClassLoader().getResource("threadedgeneticalgorithm/clusteredcoords.txt");
        File file = new File(resource.toURI());
		Boolean isDestination = false;
		Coordinates destination = new Coordinates();
		Coordinates origin;
        DateTimeFormatter dtf = DateTimeFormat.forPattern("hh:mm");
        int counter = 0;
		for (String line : FileUtils.readLines(file)) {
			counter++;
			if (line.startsWith("Going to")) {
				isDestination = true;
			} else if (!line.isEmpty()) {
				String[] userInfo = line.split(",");
				if (isDestination) {
					destination = new Coordinates(new Double(userInfo[0]), new Double(userInfo[1]));
				} else {
					System.out.println(counter);
					origin = new Coordinates(new Double(userInfo[0]), new Double(userInfo[1]));
					User user = new User(Boolean.valueOf(userInfo[2]));
					Preferences preferences = user.getPreferences();
					preferences.setSmokes(new Random().nextInt(12) < 2);
					preferences.setSmokingCondition(new Random().nextBoolean());
					preferences.setAgeGroup(new Random().nextInt(6) > 2 ? 1 : 0);
					preferences.setAgeGroupCondition(new Random().nextInt(8) < 2);
					preferences.setPreferredCarCapacity(new Random().nextInt(5) + 3);
                    DateTime startDateTime = dtf.parseDateTime(userInfo[3]);
                    Interval departure = new Interval(startDateTime, startDateTime.plusMinutes(30));
                    DateTime endDateTime = dtf.parseDateTime(userInfo[4]);
                    Interval arrival = new Interval(endDateTime.minusMinutes(30), endDateTime);
					Ride ride = user.newRide().origin(origin).destination(destination).departure(departure).arrival(arrival).createRide();
                    if (ride.isDriver()) {
						ride.getUser().setFairness(new Random().nextInt(3));
						PD.add(ride);
					} else {
						SR.add(ride);
					}
				}
				isDestination = false;
			}
			// counter++;
		}
		System.out.println(PD.size() + " potential drivers");
		System.out.println(SR.size() + " strictly riders");

		InitialSolutions initial = new InitialSolutions();
		initial.getInitialSolutions(PD, SR);
	}
}
