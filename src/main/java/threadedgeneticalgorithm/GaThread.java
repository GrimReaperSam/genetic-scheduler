package threadedgeneticalgorithm;

import client.Ride;
import geneticalgorithm.Chromosome;
import geneticalgorithm.ChromosomeHelper;
import geneticalgorithm.FitnessFunction;
import geneticalgorithm.PopulationHelper;
import initialsolution.InitialSolution;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class GaThread extends Thread implements Runnable {
	List<Ride> PD;
	List<Ride> SR;
	int numberOfDrivers;
	int threadId;
	public static AtomicInteger threadCount = new AtomicInteger(0);

	public GaThread(List<Ride> PD, List<Ride> SR, int numberOfDriver) {
		this.PD = new ArrayList<>(PD);
		this.SR = new ArrayList<>(SR);
		this.numberOfDrivers = numberOfDriver;
		threadId = threadCount.getAndIncrement();
	}

	@Override
	public void run() {
		try {
			// Class.forName("org.sqlite.JDBC");
			// GoogleMapsInfo.connection =
			// DriverManager.getConnection("jdbc:sqlite:C:/Users/fayezlahoud/workspace/SqlUpdatedGeneticAlgorithm/indexedfyp.db");
			// GoogleMapsInfo.statement = GoogleMapsInfo.connection.createStatement();
			System.out.println("Thread " + threadId);
			long startTime = System.currentTimeMillis();

			InitialSolution initialSolution = new InitialSolution();
			List<Chromosome> population = initialSolution.getInitialSolution(PD, SR, numberOfDrivers);

			/**
			 * NO INITIAL
			 */
			// List<Chromosome> population = new ArrayList<Chromosome>();
			// Collections.shuffle(PD);
			// Collections.shuffle(SR);
			// while (numberOfDrivers > 0) {
			// Chromosome cr = new Chromosome();
			// int dIndex = new Random().nextInt(PD.size()); //
			// cr.addGene(PD.remove(dIndex));
			// numberOfDrivers--;
			// population.add(cr);
			// }
			// while (!SR.isEmpty() || !PD.isEmpty()) {
			// for (Chromosome cr : population) {
			// if (!SR.isEmpty()) {
			// cr.addGene(SR.remove(0));
			// }
			// if (!PD.isEmpty()) {
			// cr.addGene(PD.remove(0));
			// }
			// }
			// }

			PrintWriter out;
			out = new PrintWriter(new FileWriter("outputfile" + threadId + ".txt"));
			for (Chromosome combo : population) {
				out.println("\nRide " + population.indexOf(combo) + " :");
				for (int z = 0; z < combo.size(); z++) {
					Ride rider = combo.getGeneAt(z);
					if (z == 0)
						out.println("Driver: Origin: (" + rider.getOrigin().getLatitude() + "," + rider.getOrigin().getLongitude() + ") Destination: ("
								+ rider.getDestination().getLatitude() + "," + rider.getDestination().getLongitude() + ")");
					else
						out.println("Passenger " + z + ": Origin: (" + rider.getOrigin().getLatitude() + "," + rider.getOrigin().getLongitude() + ") Destination: ("
								+ rider.getDestination().getLatitude() + "," + rider.getDestination().getLongitude() + ")");
				}
				out.println("\n");
			}
			for (int index = 0; index < 500; index++) {
				System.out.println(index + " " + getAvgFitness(population));
				out.println(index + " " + getAvgFitness(population));
				PopulationHelper.evolve(population);
				if (index % 10 == 0 && index != 0) {
					PopulationHelper.solveWorstCase(population);
				}
			}
			int count = 0;
			while (population.get(PopulationHelper.getMinimumFitness(population)).getFitness() < 50 && count < 100) {
				PopulationHelper.solveWorstCase(population);
				System.out.println(count);
				count++;
			}
			PopulationHelper.printStatistics(population, out);
			double minFitness = population.get(0).getFitness();
			double maxFitness = population.get(0).getFitness();
			double minTFitness = FitnessFunction.evaluateTotal(population.get(0));
			double maxTFitness = FitnessFunction.evaluateTotal(population.get(0));
			int worstIndex = 0;
			double totalFitness = 0;
			double totalTFitness = 0;
			double minIncD = FitnessFunction.increaseInDistance(population.get(0));
			double maxIncD = FitnessFunction.increaseInDistance(population.get(0));
			int above100 = 0;
			int above50 = 0;
			double totalIncD = 0;
			double minT = FitnessFunction.timePenalty(population.get(0));
			double maxT = FitnessFunction.timePenalty(population.get(0));
			double totalTimePenalty = 0;
			double minF = FitnessFunction.fairness(population.get(0));
			double maxF = FitnessFunction.fairness(population.get(0));
			double totalFairness = 0;
			int soloDrivers = 0;
			int numberOfUsers = 0;
			int smoking = 0;
			int ageGroup = 0;
			int carCapacity = 0;
			for (Chromosome chromosome : population) {
				chromosome.print(out);
				double fitness = chromosome.getFitness();
				double increaseInDistance = FitnessFunction.increaseInDistance(chromosome);
				double timePenalty = FitnessFunction.timePenalty(chromosome);
				double fairness = FitnessFunction.fairness(chromosome);
				double tFitness = FitnessFunction.evaluateTotal(chromosome);
				if (tFitness < minTFitness) {
					minTFitness = tFitness;
				}
				if (tFitness > maxTFitness) {
					maxTFitness = tFitness;
				}
				totalTFitness += tFitness;
				if (fitness < minFitness) {
					minFitness = fitness;
					worstIndex = population.indexOf(chromosome);
				}
				if (fitness > maxFitness) {
					maxFitness = fitness;
				}
				if (increaseInDistance < minIncD && increaseInDistance > 0) {
					minIncD = increaseInDistance;
				}
				if (increaseInDistance > maxIncD) {
					maxIncD = increaseInDistance;
				}
				if (increaseInDistance > 100) {
					above100++;
				}
				if (increaseInDistance > 50) {
					above50++;
				}
				if (timePenalty < minT) {
					minT = timePenalty;
				}
				if (timePenalty > maxT) {
					maxT = timePenalty;
				}
				if (fairness < minF) {
					minF = fairness;
				}
				if (fairness > maxF) {
					maxF = fairness;
				}
				totalFitness += fitness;
				totalIncD += increaseInDistance;
				totalTimePenalty += timePenalty;
				totalFairness += fairness;
				if (chromosome.size() == 1) {
					soloDrivers++;
				}
				numberOfUsers += chromosome.size();
				ArrayList<Integer> unmet = ChromosomeHelper.preferencesUnmet(chromosome);
				smoking += unmet.get(0);
				ageGroup += unmet.get(1);
				carCapacity += unmet.get(2);
			}
			long endTime = System.currentTimeMillis();
			out.println("Population size " + population.size() + " chromosomes");
			out.println("Fitness Would Have Been: " + minTFitness + "   " + maxTFitness + "    Avg " + totalTFitness / population.size());
			out.println("Fitness: " + minFitness + "   " + maxFitness + "    Avg " + totalFitness / population.size());
			out.println("IncD: " + minIncD + "   " + maxIncD + "    Avg " + totalIncD / population.size());
			out.println("Above 100 IncreasedDistance: " + above100);
			out.println("Above 50 IncreasedDistance: " + above50);
			out.println("Time penalty: " + minT + "   " + maxT + "    AVG " + totalTimePenalty / population.size());
			out.println("Fairness: " + minF + "   " + maxF + "    AVG " + totalFairness / population.size());
			out.println("Solo Drivers: " + soloDrivers);
			out.println("Users: " + numberOfUsers);
			out.println("Smoking: " + (smoking * 100.0) / numberOfUsers);
			out.println("Age Group: " + (ageGroup * 100.0) / numberOfUsers);
			out.println("Car Capacity: " + (carCapacity * 100.0) / population.size());
			out.println("That took " + (endTime - startTime) / 60000 + " minutes \n\n");

			population.get(worstIndex).print(out);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private double getAvgFitness(List<Chromosome> population) throws Exception {
		double total = 0;
		for (Chromosome c : population) {
			total += c.getFitness();
		}
		// System.out.println("Total: " + total);
		// System.out.println("PopuSize: " + population.size());
		return total / population.size();
	}
}
