package server;


import client.DatabaseHelper;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * MainHandler: Contains a multi-threaded socket server sample code.
 */
public class MainHandler extends Thread {
	final static int portNumber = 5559; // Arbitrary port number

	public static void main(String[] args) {
		try {
			new MainHandler().startServer();
		} catch (Exception e) {
			System.out.println("I/O failure: " + e.getMessage());
			e.printStackTrace();
		}

	}

	public void startServer() throws Exception {
		ServerSocket serverSocket = null;
		boolean listening = true;

		try {
			serverSocket = new ServerSocket(portNumber);
		} catch (IOException e) {
			System.err.println("Could not listen on port: " + portNumber);
			System.exit(-1);
		}

		while (listening) {
			handleClientRequest(serverSocket);
		}

		serverSocket.close();
	}

	private void handleClientRequest(ServerSocket serverSocket) {
		try {
			new ConnectionRequestHandler(serverSocket.accept()).run();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Handles client connection requests.
	 */
	public class ConnectionRequestHandler implements Runnable {
		private Socket socket = null;
		private DataOutputStream outDataStream;
		private DataInputStream inDataStream;
		private boolean loggedIn;

		public ConnectionRequestHandler(Socket socket) {
			this.socket = socket;
		}

		public void run() {
			System.out.println("Client connected to socket: " + socket.toString());
			try {
				loggedIn = false;
				outDataStream = new DataOutputStream(socket.getOutputStream());
				inDataStream = new DataInputStream(socket.getInputStream());
				while (!loggedIn) {
					String input = inDataStream.readUTF();
					System.out.println(input);
					String[] split = input.split("-");
					if (split[0].equals("Login")) {
						handleLogin(split[1], split[2]);
					} else if (split[0].equals("Register") && !DatabaseHelper.checkIfUsernameExists("user_info", split[2])) {
						int i = 0;
						for (String s : split) {
							System.out.println(i + "   " + s);
							i++;
						}
						DatabaseHelper.insertIntoDatabase("login", split[2], split[7]);
						String hasCar = split[8].equals("Y") ? "true" : "false";
						String likesSmoking;
						if (Boolean.valueOf(hasCar)) {
							likesSmoking = split[16].equals("Y") ? "true" : "false";
							DatabaseHelper.insertIntoDatabase("user_info", split[2], split[1], split[3], split[5], split[4], hasCar, likesSmoking, split[18], split[17], split[6]);
							DatabaseHelper.insertIntoDatabase("driver_info", split[2], split[15], split[13], split[14], split[9], split[10], split[12], split[11]);
						} else {
							likesSmoking = split[9].equals("Y") ? "true" : "false";
							DatabaseHelper.insertIntoDatabase("user_info", split[2], split[1], split[3], split[5], split[4], hasCar, likesSmoking, split[11], split[10], split[6]);
						}
						outDataStream.writeBoolean(true);
					} else {
						outDataStream.writeBoolean(false);
					}
				}
				System.out.println("LOGGED IN");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private void handleLogin(String username, String password) throws Exception {
			String hashTable;
			if (DatabaseHelper.checkIfUsernameExists("login", username)) {
				hashTable = DatabaseHelper.getFromDatabase("login", username, "password");
			} else {
				hashTable = "";
			}
			System.out.println(hashTable);
			if (password.equals(hashTable)) {
				outDataStream.writeBoolean(true);
				loggedIn = true;
			} else {
				outDataStream.writeBoolean(false);
			}
		}
	}
}
